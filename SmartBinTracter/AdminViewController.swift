//
//  AdminViewController.swift
//  SmartBinTracter
//
//  Created by Chandresh on 5/11/2015.
//  Copyright © 2015 IndianTech. All rights reserved.
//

import UIKit

class AdminViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var userDataArray: NSMutableArray! = [];
    @IBOutlet var binsTableView: UITableView!

    var binsDataArray = [BinData]();
    
    //http://130.56.252.236/webservices/SelectDrivers.php
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.navigationItem.hidesBackButton = true
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        binsDataArray = [BinData]();
        downloadBinData()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCellWithIdentifier("BinDataCell", forIndexPath: indexPath)
        
        let aBin:BinData = binsDataArray[indexPath.row] 
        
        cell.textLabel?.text = "Bin From: " + aBin.binLatitude + "," + aBin.binLongitude
        cell.detailTextLabel?.text = "Bin Depth: " + aBin.binDepth
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.binsDataArray.count>0)
        {
            return self.binsDataArray.count
        }
        else
        {
            return 0;
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    func downloadBinData()
    {
        //http://stackoverflow.com/questions/30739473/nsurlsession-nsurlconnection-http-load-failed-on-ios-9
        //http://stackoverflow.com/questions/24016142/how-to-make-an-http-request-in-swift
        //http://stackoverflow.com/questions/30737262/swift-2-call-can-throw-but-it-is-not-
        
        let url = NSURL(string: "http://130.56.252.236/webservices/SelectAllBinsData.php")!
        let task = NSURLSession.sharedSession().dataTaskWithURL(url) { (data, response, error) in
            let reply = NSString(data: data!, encoding: NSUTF8StringEncoding)
            
            if data != nil
            {
                do
                {
                    let parsedObject: AnyObject? = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
                    let response = parsedObject as? NSArray;
                    //convert response to array and grab all data
                    self.initalizeBinDataFromServer(response)
                    
                }
                catch let error as NSError
                {
                    print(error)
                }
            }
        }
        task.resume();
    }
    
    // MARK: Array initialization from JSON response
    //convert the response data in a format which can be used later to show markers on the map
    func initalizeBinDataFromServer(response:NSArray!)
    {
        for data:AnyObject in response
        {
            switch data
            {
            case let data as NSDictionary:
                let binLatitude:String! = data.valueForKey("Latitude") as! String
                let binLongitude:String! = data.valueForKey("Longitude") as! String
                let binDepth:String! = data.valueForKey("BinDepth") as! String
                let binUpdatedTime:String! = data.valueForKey("UpdatedTime") as! String
                let isCollected:String! = data.valueForKey("IsCollected") as! String
                
                //BinData is a class. initialize for each instance of response and add it to the array.
                let aBinData:BinData = BinData(BinWithData: binLatitude, longitude: binLongitude, depth: binDepth, time: binUpdatedTime, isCollected: isCollected)
                
                binsDataArray.append(aBinData)
                
            default:
                print("not a dictionary")
            }
        }
        
        if(self.binsDataArray.count>0)
        {
            dispatch_async(dispatch_get_main_queue(), {
                [self.binsTableView.reloadData()]})
            
        }
    }


    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "UpdateBinSegue"
        {
            let updateBinController = segue.destinationViewController as! UpdateBinViewController
            let indexPath = self.binsTableView.indexPathForSelectedRow!
            updateBinController.binData = self.binsDataArray[indexPath.row]
        }
    }
    

}
