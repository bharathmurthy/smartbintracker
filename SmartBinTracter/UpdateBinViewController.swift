//
//  UpdateBinViewController.swift
//  SmartBinTracter
//
//  Created by Chandresh on 5/11/2015.
//  Copyright © 2015 IndianTech. All rights reserved.
//

import UIKit

class UpdateBinViewController: UIViewController {

    @IBOutlet var binLatitudeTextField: UITextField!
    @IBOutlet var binLongitudeTextField: UITextField!
    @IBOutlet var binDepthTextField: UITextField!
    @IBOutlet var binUpdatedTimeTextField: UITextField!
    
    var binData = BinData()
    var oldLat:String = ""
    var oldLong:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        oldLat = binData.binLatitude
        oldLong = binData.binLongitude
        
        self.binLatitudeTextField.text = binData.binLatitude
        self.binLongitudeTextField.text = binData.binLongitude
        self.binDepthTextField.text = binData.binDepth
        self.binUpdatedTimeTextField.text = binData.binUpdateTime
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func updateBinData(sender: AnyObject)
    {
        if(self.validateTextInput(self.binLatitudeTextField.text!) &&
        self.validateTextInput(self.binLongitudeTextField.text!) &&
        self.validateTextInput(self.binDepthTextField.text!) &&
        self.validateTextInput(self.binUpdatedTimeTextField.text!))
        {
        let request = NSMutableURLRequest(URL: NSURL(string: "http://130.56.252.236/webservices/UpdateBin.php")!)
        request.HTTPMethod = "POST"
        let latitude = self.binLatitudeTextField.text
        let longitude = self.binLongitudeTextField.text
        let bindepth = self.binDepthTextField.text
        let binupdatedtime = self.binUpdatedTimeTextField.text
        
        let oldlatPostString = "oldlat=" + oldLat
        let oldlongPostString = "oldlong=" + oldLong
        let latitudePostString = "latitude=" + latitude!
        let longitudePostString = "longitude=" + longitude!
        let depthPostString = "bindepth=" + bindepth!
        let timePostString = "updatedtime=" + binupdatedtime!
        
        let postString = oldlatPostString + "&" + oldlongPostString + "&" + latitudePostString + "&" + longitudePostString + "&" + depthPostString + "&" + timePostString
        
        request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            
            if error != nil {
                print("error=\(error)")
                return
            }
            
            print("response = \(response)")
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            
            if responseString == "1"
            {
                dispatch_async(dispatch_get_main_queue(),{
                    let refreshAlert = UIAlertController(title: "Success!!", message: "Bin data updated.", preferredStyle: UIAlertControllerStyle.Alert)
                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Cancel, handler: { (action: UIAlertAction!) in
                    }))
                    self.presentViewController(refreshAlert, animated: true, completion: nil)
                    
                })
                
                
               
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(),{
                    
                let refreshAlert = UIAlertController(title: "Error!!", message: "Bin data cannot be updated. Please try again later.", preferredStyle: UIAlertControllerStyle.Alert)
                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Cancel, handler: { (action: UIAlertAction!) in
                    }))
                self.presentViewController(refreshAlert, animated: true, completion: nil)
                })
            }
        }
        task.resume()
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(),{
                
            let refreshAlert = UIAlertController(title: "No Data!!", message: "Please add all values.", preferredStyle: UIAlertControllerStyle.Alert)
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Cancel, handler: { (action: UIAlertAction!) in
                }))
            self.presentViewController(refreshAlert, animated: true, completion: nil)
            })
        }
    }
    
    func validateTextInput(textString:String) -> Bool
    {
        let stringData:String! = textString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        if stringData.characters.count>0
        {
            return true
        }
        
        return false
    }
}
