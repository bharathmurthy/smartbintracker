//
//  LoginViewController.swift
//  SmartBinTracter
//
//  Created by Chandresh on 5/11/2015.
//  Copyright © 2015 IndianTech. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    // MARK: - Class Variables
    
    @IBOutlet var userEmailIdTextField: UITextField!
    @IBOutlet var userPasswordTextField: UITextField!
    
    var userResultArray: NSMutableArray! = [];

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        userResultArray = NSMutableArray();
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Button Clicks
    
    @IBAction func loginWithCredentials(sender: AnyObject)
    {
        
        if(self.validateTextInput(self.userEmailIdTextField.text!) &&
            self.validateTextInput(self.userPasswordTextField.text!))
        {
            self.getUserDetailsFromInput()
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(),{
                let refreshAlert = UIAlertController(title: "No Data!!", message: "Please add userid and password.", preferredStyle: UIAlertControllerStyle.Alert)
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Cancel, handler: { (action: UIAlertAction!) in
                }))
                self.presentViewController(refreshAlert, animated: true, completion: nil)
            })
        }
        
        
        
    }
    
    // MARK: - Http Request & Response
    
    func getUserDetailsFromInput()
    {
        //http://stackoverflow.com/questions/26364914/http-request-in-swift-with-post-method
        let request = NSMutableURLRequest(URL: NSURL(string: "http://130.56.252.236/webservices/SelectLoginUser.php")!)
        request.HTTPMethod = "POST"
        let userEmailAddress = self.userEmailIdTextField.text
        let userPassword = self.userPasswordTextField.text
        
        let userEmailPostString = "emailid=" + userEmailAddress!
        let userPasswordPostString = "password=" + userPassword!
        
        let postString = userEmailPostString + "&" + userPasswordPostString
        
        request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            
            if error != nil {
                print("error=\(error)")
                return
            }
            
            print("response = \(response)")
            
            if data != nil
            {
                do
                {
                    let parsedObject: AnyObject? = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
                    let responseArray = parsedObject as? NSArray;
                    self.doUserIntialization(responseArray)
                    print(responseArray?.count)
                    
                }
                catch let error as NSError
                {
                    print(error)
                    dispatch_async(dispatch_get_main_queue(),{
                        let refreshAlert = UIAlertController(title: "Login Failed!!", message: "Please check your userid and password.", preferredStyle: UIAlertControllerStyle.Alert)
                        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Cancel, handler: { (action: UIAlertAction!) in
                        }))
                        self.presentViewController(refreshAlert, animated: true, completion: nil)
                    })
                }
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(),{
                    let refreshAlert = UIAlertController(title: "Login Failed!!", message: "Please check your userid and password.", preferredStyle: UIAlertControllerStyle.Alert)
                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Cancel, handler: { (action: UIAlertAction!) in
                    }))
                    self.presentViewController(refreshAlert, animated: true, completion: nil)
                })
            }
        
        }
        task.resume()
    }
    
    // MARK: - User Initialization For Segue
    
    func doUserIntialization(response:NSArray!)
    {
        for data:AnyObject in response
        {
            switch data
            {
            case let data as NSDictionary:
                let userId:String! = data.valueForKey("UserId") as! String
                let userFname:String! = data.valueForKey("FirstName") as! String
                let userLname:String! = data.valueForKey("LastName") as! String
                let userEmailID:String! = data.valueForKey("EmailAddress") as! String
                let userPassword:String! = data.valueForKey("Password") as! String
                let userType:String! = data.valueForKey("UserType") as! String
                
                let aUserData:User = User(UserWithID: userId, fname: userFname, lname: userLname, email: userEmailID, password: userPassword, type: userType)
                
                userResultArray.addObject(aUserData)
                self.setTheUserForAppAndPerformSegue()
            default:
                print("not a dictionary")
            }
        }
    }
    
    // MARK: - Performing segue
   
    func setTheUserForAppAndPerformSegue()
    {
        let aLoggedInUser:User = self.userResultArray[0] as! User
        
        if aLoggedInUser.userType == "Admin"
        {
            dispatch_async(dispatch_get_main_queue(), {
               [self .performSegueWithIdentifier("ShowAdminSegue", sender: self)]})
            
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), {
                [self .performSegueWithIdentifier("ShowDriverSegue", sender: self)]})
                   }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "ShowAdminSegue"
        {
            let tabController = segue.destinationViewController as! UITabBarController
           // let firstab = tabController.viewControllers?[0] as? UINavigationController
            let adminController = tabController.viewControllers?[0] as! AdminViewController
            adminController.userDataArray = self.userResultArray
            
        }
        else
        {
            var driverController = segue.destinationViewController as! ViewController

        }
    }
    
    func validateTextInput(textString:String) -> Bool
    {
        let stringData:String! = textString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        if stringData.characters.count>0
        {
            return true
        }
        
        return false
    }

}
