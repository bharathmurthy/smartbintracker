//
//  User.swift
//  SmartBinTracter
//
//  Created by Chandresh on 5/11/2015.
//  Copyright © 2015 IndianTech. All rights reserved.
//

import UIKit

class User: NSObject
{
    var userId: String!
    var userFirstName: String!
    var userLastName: String!
    var userEmailAddress: String!
    var userPassword:String!
    var userType: String!
    
    override init() {
        self.userId = ""
        self.userFirstName = ""
        self.userLastName = ""
        self.userEmailAddress = ""
        self.userPassword = ""
        self.userType = ""

    }
    
    init(UserWithID userid:String!, fname:String!, lname:String!, email:String!, password:String!, type:String! )
    {
        self.userId = userid
        self.userFirstName = fname
        self.userLastName = lname
        self.userEmailAddress = email
        self.userPassword = password
        self.userType = type
    }


}
