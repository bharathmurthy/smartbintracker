//
//  ViewController.swift
//  SmartBinTracter
//
//  Created by Kumar Bharath Hassan narasimha murthy on 29/10/2015.
//  Copyright © 2015 IndianTech. All rights reserved.
//

import UIKit
import GoogleMaps

//trying to sync 12
class ViewController: UIViewController, CLLocationManagerDelegate
{
    static var kMDDirectionsURL:String = "http://maps.googleapis.com/maps/api/directions/json?"
    
    // MARK: Class Variables
    @IBOutlet var binDepthTextField: UITextField!
    var binResultArray: NSMutableArray! = [];
    @IBOutlet var mapTypeSelector: UISegmentedControl!
    @IBOutlet var mapView: GMSMapView!
    let locationManager = CLLocationManager()
    
    // MARK: Button Clicks
    @IBAction func mapType(sender: AnyObject) {
        switch(mapTypeSelector.selectedSegmentIndex){
        case 0:
            self.mapView.mapType = GoogleMaps.kGMSTypeNormal;
            break;
        case 1:
            self.mapView.mapType = GoogleMaps.kGMSTypeTerrain;
            break;
        case 2:
            self.mapView.mapType = GoogleMaps.kGMSTypeSatellite;
            break;
        case 3:
            self.mapView.mapType = GoogleMaps.kGMSTypeHybrid;
            break;
        default:break;
            
        }
    }
    
    @IBAction func searchBinByDepth(sender: AnyObject)
    {
        if(binDepthTextField.text?.characters.count>0)
        {
            binResultArray = NSMutableArray()
            //http://stackoverflow.com/questions/26364914/http-request-in-swift-with-post-method
            let request = NSMutableURLRequest(URL: NSURL(string: "http://130.56.252.236/webservices/SelectBinsByDepth.php")!)
            request.HTTPMethod = "POST"
            
            let postString = "bindepth=" + binDepthTextField.text!
            
            request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
                data, response, error in
                
                if error != nil {
                    print("error=\(error)")
                    return
                }
                
                print("response = \(response)")
                
                if data != nil
                {
                    do
                    {
                        let parsedObject: AnyObject? = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
                        let response = parsedObject as? NSArray;
                        
                        dispatch_async(dispatch_get_main_queue(),{self.mapView.clear()})
                        //convert response to array and grab all data
                        self.initalizeBinDataFromServer(response)
                        // print(response?.count)
                        // print(self.binResultArray.count)
                        self.showBinLocationsOnTheMap()
                    }
                    catch let error as NSError
                    {
                        print(error)
                    }
                }
                
                
            }
            task.resume()

        }
        

    }
    
    
    @IBAction func getDirections(sender: AnyObject)
    {
        if binResultArray.count<=8
        {
            getDirections()
        }
        else
        {
            
            dispatch_async(dispatch_get_main_queue(),{
                
                let refreshAlert = UIAlertController(title: "Too much!!", message: "Only 8 bins can be tracked at a time.", preferredStyle: UIAlertControllerStyle.Alert)
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Cancel, handler: { (action: UIAlertAction!) in
                }))
                self.presentViewController(refreshAlert, animated: true, completion: nil)
            })
           
        }
        
    }
    // MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.navigationItem.hidesBackButton = true
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
       
        downloadBinData()
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: HTTP Request and Response
    // download all bin data from the server.
    func downloadBinData()
    {
        //http://stackoverflow.com/questions/30739473/nsurlsession-nsurlconnection-http-load-failed-on-ios-9
        //http://stackoverflow.com/questions/24016142/how-to-make-an-http-request-in-swift
        //http://stackoverflow.com/questions/30737262/swift-2-call-can-throw-but-it-is-not-
        
        
        /** new webservice to get bin data by user choice.
        * requires post parameter called = 'bindepth' for ex. bindepth=80
        * add it as a post paramater of http request. see the add new bin button click for example.
        * 
        *  http://130.56.252.236/webservices/SelectBinsByDepth.php
        */
        
        
        let url = NSURL(string: "http://130.56.252.236/webservices/SelectAllBins.php")!
        let task = NSURLSession.sharedSession().dataTaskWithURL(url) { (data, response, error) in
            let reply = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print(reply)
           if data != nil
           {
                do
                {
                    let parsedObject: AnyObject? = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
                    let response = parsedObject as? NSArray;
                    //convert response to array and grab all data
                    self.initalizeBinDataFromServer(response)
                   // print(response?.count)
                   // print(self.binResultArray.count)
                    self.showBinLocationsOnTheMap()
                    
                    
                }
                catch let error as NSError
                {
                      print(error)
                }
            }
        }
        task.resume();
   }
    
    // MARK: Array initialization from JSON response
    //convert the response data in a format which can be used later to show markers on the map
    func initalizeBinDataFromServer(response:NSArray!)
    {
        for data:AnyObject in response
        {
            switch data
            {
            case let data as NSDictionary:
                let binLatitude:String! = data.valueForKey("Latitude") as! String
                let binLongitude:String! = data.valueForKey("Longitude") as! String
                let binDepth:String! = data.valueForKey("BinDepth") as! String
                let binUpdatedTime:String! = data.valueForKey("UpdatedTime") as! String
                let isCollected:String! = data.valueForKey("IsCollected") as! String
                
                //BinData is a class. initialize for each instance of response and add it to the array.
                let aBinData:BinData = BinData(BinWithData: binLatitude, longitude: binLongitude, depth: binDepth, time: binUpdatedTime, isCollected: isCollected)
                
                binResultArray.addObject(aBinData);
                
            default:
                print("not a dictionary")
            }
        }
    }
    
    // MARK: Map Markers
    func showBinLocationsOnTheMap()
    {
        // gms marker must be called from main UI thread. So, dispatch_async is required.
        //http://stackoverflow.com/questions/31287149/how-to-add-a-gmsmarker-for-a-gmsmapview-in-swift
        dispatch_async(dispatch_get_main_queue(),
            {
                // for each BinData instance show marker on the map
                for data:AnyObject in self.binResultArray
                {
                    switch data{
                    case let data as BinData:
                    let marker = GMSMarker()
                        marker.position = CLLocationCoordinate2DMake(Double(data.binLatitude)!, Double(data.binLongitude)!)
                        marker.title = "Filled " + data.binDepth + "%"
                        marker.snippet = "Time Updated " + data.binUpdateTime
                        marker.icon = UIImage(named: "./images/dustbin-1.png")
                        marker.map = self.mapView
                        
                    default:
                        print("not an object")
                    }
                }
        })
        
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        // 3
        if status == .AuthorizedWhenInUse {
            
            // 4
            locationManager.startUpdatingLocation()
            
            //5
            mapView.myLocationEnabled = true
            mapView.settings.myLocationButton = true
        
        }
    }
    
    // 6
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            
            // 7
            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 20, bearing: 0, viewingAngle: 0)
            
            // 8
            locationManager.stopUpdatingLocation()
            

        }
        
    }
    
    
    func getDirections(){
        
        let currentUserLat = String(locationManager.location!.coordinate.latitude)
        let currentUserLong = String(locationManager.location!.coordinate.longitude)
        
        let destinationLat = (binResultArray.lastObject as! BinData).binLatitude
        let destinationLong = (binResultArray.lastObject as! BinData).binLongitude
        
        var wayPointsString:String = ""
        
        for var i = 0; i < binResultArray.count; i++  {
            let binInArray = binResultArray.objectAtIndex(i) as! BinData
            print(binInArray.binLatitude)
            wayPointsString += binInArray.binLatitude + ","
            wayPointsString += binInArray.binLongitude + "%7C"
        }
        
        wayPointsString.removeAtIndex(wayPointsString.endIndex.predecessor())
        wayPointsString.removeAtIndex(wayPointsString.endIndex.predecessor())
        wayPointsString.removeAtIndex(wayPointsString.endIndex.predecessor())
        print(wayPointsString)
        //"&waypoints="+wayPointsString+
        let url = NSURL(string: "https://maps.googleapis.com/maps/api/directions/json?origin="+currentUserLat+","+currentUserLong+"&destination="+destinationLat+","+destinationLong+"&waypoints=optimize:true%7C"+wayPointsString+"&key=AIzaSyCbOr5M5VxM5KvJg8ehau5O95l-GewUz2A")
        let task = NSURLSession.sharedSession().dataTaskWithURL(url!) { (data, response, error) in
            let reply = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print(reply)
            if data != nil
            {
                do
                {
                    let parsedObject = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
                    let waypoints = parsedObject.valueForKey("geocoded_waypoints")
                    print(waypoints)
                    
                    let routes = parsedObject.valueForKey("routes")![0] as! NSDictionary
                    let route = routes.valueForKey("overview_polyline") as! NSDictionary
                    let overview_route = route.objectForKey("points") as! String
                    let path = GMSPath(fromEncodedPath: overview_route)
                    dispatch_async(dispatch_get_main_queue(),
                        {
                            let polyLine = GMSPolyline(path: path)
                            polyLine.strokeWidth = CGFloat(10.0)
                            polyLine.strokeColor = UIColor.brownColor()
                            polyLine.map = self.mapView
                            

                            
                    })
                }
                catch let error as NSError
                {
                    print(error)
                }
            }
        }
        task.resume();

        
    }
    
    
    
}

