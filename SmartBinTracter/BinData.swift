//
//  BinData.swift
//  SmartBinTracter
//
//  Created by Chandresh on 4/11/2015.
//  Copyright © 2015 IndianTech. All rights reserved.
//

import UIKit

class BinData: NSObject
{
    var binLatitude: String!
    var binLongitude: String!
    var binDepth: String!
    var binUpdateTime: String!
    var binIsCollected: String!
    
    init(BinWithData latitude:String!, longitude:String!, depth:String!, time:String!, isCollected:String! )
    {
        self.binLatitude = latitude
        self.binLongitude = longitude
        self.binDepth = depth
        self.binUpdateTime = time
        self.binIsCollected = isCollected
    }
    
    override init() {
        self.binLatitude = ""
        self.binLongitude = ""
        self.binDepth = ""
        self.binUpdateTime = ""
        self.binIsCollected = ""
    }
}
