//
//  AddNewDriverViewController.swift
//  SmartBinTracter
//
//  Created by Chandresh on 5/11/2015.
//  Copyright © 2015 IndianTech. All rights reserved.
//

import UIKit

class AddNewDriverViewController: UIViewController {
    @IBOutlet var userFNameTextField: UITextField!
    @IBOutlet var userLastNameTextField: UITextField!
    @IBOutlet var userEmailAddressTextField: UITextField!
    @IBOutlet var userPasswordTextField: UITextField!
    @IBOutlet var userConfirmPasswordTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func saveDriverData(sender: AnyObject)
    {
        if(self.validateTextInput(self.userFNameTextField.text!) &&
        self.validateTextInput(self.userLastNameTextField.text!) &&
        self.validateTextInput(self.userEmailAddressTextField.text!) &&
        self.validateTextInput(self.userPasswordTextField.text!) &&
        self.validateTextInput(self.userConfirmPasswordTextField.text!))
        {
            if self.userPasswordTextField.text == self.userConfirmPasswordTextField.text
            {
                let request = NSMutableURLRequest(URL: NSURL(string: "http://130.56.252.236/webservices/InsertDriver.php")!)
                request.HTTPMethod = "POST"
                let fname = self.userFNameTextField.text
                let lname = self.userLastNameTextField.text
                let email = self.userEmailAddressTextField.text
                let password = self.userPasswordTextField.text
                
                let fnamePostString = "firstName=" + fname!
                let lnamePostString = "lastName=" + lname!
                let emailPostString = "emailAddress=" + email!
                let passwordPostString = "password=" + password!
                
                let postString = fnamePostString + "&" + lnamePostString + "&" + emailPostString + "&" + passwordPostString
                
                request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
                let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
                    data, response, error in
                    
                    if error != nil {
                        print("error=\(error)")
                        return
                    }
                    
                    print("response = \(response)")
                    
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseString = \(responseString)")
                   
                    if responseString == "1"
                    {
                        dispatch_async(dispatch_get_main_queue(),{
                            //http://stackoverflow.com/questions/33041416/uialertcontroller-actionsheet-issue-over-ios9
                            let refreshAlert = UIAlertController(title: "Success!!", message: "New Driver added.", preferredStyle: UIAlertControllerStyle.Alert)
                            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Cancel, handler: { (action: UIAlertAction!) in
                            }))
                            self.presentViewController(refreshAlert, animated: true, completion: nil)
                            
                        })
                    }
                    else
                    {
                        dispatch_async(dispatch_get_main_queue(),{
                            let refreshAlert = UIAlertController(title: "Error!!", message: "Driver data cannot be added. Please try again later.", preferredStyle: UIAlertControllerStyle.Alert)
                            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Cancel, handler: { (action: UIAlertAction!) in
                            }))
                            self.presentViewController(refreshAlert, animated: true, completion: nil)
                            
                        })                    }
                    
                }
                task.resume()
                
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(),{
                    let refreshAlert = UIAlertController(title: "Password!!", message: "Password did not match. Try again.", preferredStyle: UIAlertControllerStyle.Alert)
                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Cancel, handler: { (action: UIAlertAction!) in
                    }))
                    self.presentViewController(refreshAlert, animated: true, completion: nil)
                    
                })

            
                
            }
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(),{
                
                let refreshAlert = UIAlertController(title: "No Data!!", message: "Please add all values.", preferredStyle: UIAlertControllerStyle.Alert)
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Cancel, handler: { (action: UIAlertAction!) in
                }))
                self.presentViewController(refreshAlert, animated: true, completion: nil)
            })
        }
     
    }
    
    func validateTextInput(textString:String) -> Bool
    {
        let stringData:String! = textString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        if stringData.characters.count>0
        {
            return true
        }
        
        return false
    }
}
