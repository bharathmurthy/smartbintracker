//
//  DriverListViewController.swift
//  SmartBinTracter
//
//  Created by Chandresh on 5/11/2015.
//  Copyright © 2015 IndianTech. All rights reserved.
//

import UIKit

class DriverListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

     var userDataArray: NSMutableArray! = []
    @IBOutlet var driversTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        userDataArray = NSMutableArray()

        downloadDriverData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.userDataArray.count>0
        {
            return self.userDataArray.count
        }
        else
        {
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("DriverDataCell", forIndexPath: indexPath)
        
        let aUser:User = self.userDataArray[indexPath.row] as! User
        
        
        cell.textLabel?.text = "Email id " + aUser.userEmailAddress
        cell.detailTextLabel?.text = "Name " + aUser.userFirstName + " " + aUser.userLastName
        return cell

    }

    
    func downloadDriverData()
    {
        //http://stackoverflow.com/questions/30739473/nsurlsession-nsurlconnection-http-load-failed-on-ios-9
        //http://stackoverflow.com/questions/24016142/how-to-make-an-http-request-in-swift
        //http://stackoverflow.com/questions/30737262/swift-2-call-can-throw-but-it-is-not-
        
        let url = NSURL(string: "http://130.56.252.236/webservices/SelectDrivers.php")!
        let task = NSURLSession.sharedSession().dataTaskWithURL(url) { (data, response, error) in
            
            if data != nil
            {
                do
                {
                    let parsedObject: AnyObject? = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
                    let response = parsedObject as? NSArray;
                    //convert response to array and grab all data
                    self.initalizeDriverDataFromServer(response)
                    
                }
                catch let error as NSError
                {
                    print(error)
                }
            }
        }
        task.resume();
    }
    
    // MARK: Array initialization from JSON response
    //convert the response data in a format which can be used later to show markers on the map
    func initalizeDriverDataFromServer(response:NSArray!)
    {
        for data:AnyObject in response
        {
            switch data
            {
                case let data as NSDictionary:
                    let userId:String! = data.valueForKey("UserId") as! String
                    let userFname:String! = data.valueForKey("FirstName") as! String
                    let userLname:String! = data.valueForKey("LastName") as! String
                    let userEmailID:String! = data.valueForKey("EmailAddress") as! String
                    let userPassword:String! = data.valueForKey("Password") as! String
                    let userType:String! = data.valueForKey("UserType") as! String
                
                    let aUserData:User = User(UserWithID: userId, fname: userFname, lname: userLname, email: userEmailID, password: userPassword, type: userType)
                
                    self.userDataArray.addObject(aUserData)
                
                default:
                    print("not a dictionary")
            }
        }
        
        if(self.userDataArray.count>0)
        {
            dispatch_async(dispatch_get_main_queue(), {
                [self.driversTableView.reloadData()]})
            
        }
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "UpdateDriverSegue"
        {
            let updateDriverController = segue.destinationViewController as! UpdateDriverViewController
            let indexPath = self.driversTableView.indexPathForSelectedRow!
            updateDriverController.userData = self.userDataArray[indexPath.row] as! User

        }
    }
    

}
